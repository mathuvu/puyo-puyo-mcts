import random

import numpy as np

import puyocore as core
from gym_puyopuyo.field import TallField

GAMMA = 0.95
BOTTOM_MOVE_TRANSLATION = {
    0:6, 1:8, 9:4, 22:1, 16:9, 23:3, 7:0, 24:5, 15:7, 8:2 
}
TALL_TSU_MOVE_TRANSLATION = {
    20:16, 18:19, 0:12, 1:14, 2:16, 3:18, 4:20, 7:0, 8:2, 9:4, 10:6, 11:8, 12:10,
    15:13, 16:15, 17:17, 18:19, 19:21, 22:1, 23:3, 24:5, 25:7, 26:9, 27:11
}

def tree_search_actions(state, depth, factor=0.22, occupation_threshold=0.0):
    colors = []
    for deal in state.deals[1:]:
        colors.extend(deal)

    action_mask = 0
    for action in state.actions:
        action_mask |= 1 << state._validation_actions.index(action)

    search_args = [
        state.num_layers,
        state.has_garbage,
        action_mask,
        colors,
        depth - 1,
        factor,
    ]
    search_fun = core.bottom_tree_search
    if isinstance(state.field, TallField):
        search_args.insert(1, state.tsu_rules)
        search_args.insert(1, state.width)
        search_fun = core.tall_tree_search

    base_popcount = state.field.popcount
    prevent_chains = (base_popcount < occupation_threshold * state.width * state.height)

    best_indices = []
    best_score = float("-inf")

    possible_indices = []
    possible_score = float("-inf")
    for index, (child, score) in enumerate(state.get_children(True)):
        if not child:
            continue

        args = [child.field.data] + search_args
        tree_score = search_fun(*args)

        child_score = score + GAMMA * tree_score

        if prevent_chains and child.field.popcount < base_popcount:
            if child_score > possible_score:
                possible_indices = [index]
                possible_score = child_score
            elif child_score == possible_score:
                possible_indices.append(index)
        else:
            if child_score > best_score:
                best_indices = [index]
                best_score = child_score
            elif child_score == best_score:
                best_indices.append(index)
    return best_indices or possible_indices or [np.random.randint(0, len(state.actions))]

def flat_mc_actions(state, depth, n, heuristic, gamma=0.80):
    colors = []
    for deal in state.deals:
        colors.extend(deal)
    
    action_mask = 0
    for action in state.actions:
        action_mask |= 1 << state._validation_actions.index(action)

    search_args = [
        n,
        depth,
        gamma,
        state.field.data,
        state.num_layers,
        colors,
        state.has_garbage,
        action_mask
    ]
    search_fun = lambda x: BOTTOM_MOVE_TRANSLATION[core.bottom_flat_mc(*x)]
    if isinstance(state.field, TallField):
        search_args.append(state.tsu_rules)
        search_args.insert(4, state.width)
        search_fun = lambda x: TALL_TSU_MOVE_TRANSLATION[core.tall_flat_mc(*x)]
    
    return search_fun(search_args)

def nmcs_actions(state, depth, n_playout, level, heuristic, gamma=0.8):
    colors = []
    for deal in state.deals:
        colors.extend(deal)
    
    action_mask = 0
    for action in state.actions:
        action_mask |= 1 << state._validation_actions.index(action)

    search_args = [
        state.field.data,
        state.num_layers,
        colors,
        state.has_garbage,
        action_mask,
        gamma,
        depth,
        n_playout,
        level,
        heuristic
    ]
    search_fun = lambda x: BOTTOM_MOVE_TRANSLATION[core.bottom_nmcs(*x)]
    if isinstance(state.field, TallField):
        search_args.insert(4,state.tsu_rules)
        search_args.insert(1, state.width)
        search_fun = lambda x: TALL_TSU_MOVE_TRANSLATION[core.tall_nmcs(*x)]
    
    return search_fun(search_args)

def nrpa_actions(state, depth, level, gamma=0.8,n=100,PolicyType=1,zeta=1):
    colors = []
    for deal in state.deals:
        colors.extend(deal)
    
    action_mask = 0
    for action in state.actions:
        action_mask |= 1 << state._validation_actions.index(action)
        
    search_args = [
        state.field.data,
        state.num_layers,
        colors,
        state.has_garbage,
        action_mask,
        gamma,
        depth,
        level,
        n,
        PolicyType,
        zeta
    ]
    #bottom_nmcs(puyos_t *board, int num_layers, int num_deals, int *colors, int has_garbage, bitset_t action_mask, double gamma, int depth, int level, int* best_moves, int *best_colors)
    search_fun = lambda x: BOTTOM_MOVE_TRANSLATION[core.bottom_nrpa(*x)]
    if isinstance(state.field, TallField):#to modify
        search_args.append(state.tsu_rules)
        search_args.append(state.width)
        search_fun = lambda x: TALL_TSU_MOVE_TRANSLATION[core.tall_nrpa(*x)]
    
    return search_fun(search_args)

def gnrpa_actions(state, depth, level, gamma=0.8,n=100,PolicyType=1,tau=1):
    colors = []
    for deal in state.deals:
        colors.extend(deal)
    
    action_mask = 0
    for action in state.actions:
        action_mask |= 1 << state._validation_actions.index(action)
        
    search_args = [
        state.field.data,
        state.num_layers,
        colors,
        state.has_garbage,
        action_mask,
        gamma,
        depth,
        level,
        n,
        PolicyType,
        tau
    ]
    #bottom_nmcs(puyos_t *board, int num_layers, int num_deals, int *colors, int has_garbage, bitset_t action_mask, double gamma, int depth, int level, int* best_moves, int *best_colors)
    search_fun = lambda x: BOTTOM_MOVE_TRANSLATION[core.bottom_gnrpa(*x)]
    if isinstance(state.field, TallField):#to modify
        search_args.append(state.tsu_rules)
        search_args.append(state.width)
        search_fun = lambda x: TALL_TSU_MOVE_TRANSLATION[core.tall_gnrpa(*x)]
    
    return search_fun(search_args)

class GNRPAAgent(object):
    def __init__(self, depth, level, gamma=0.8,n=100,PolicyType=1,tau=1):
        self.depth = depth
        self.level = level
        self.gamma = gamma
        self.n = n
        self.PolicyType = PolicyType
        self.tau = tau

    def get_action(self, state):
        return gnrpa_actions(state, self.depth, self.level, self.gamma,self.n,self.PolicyType,self.tau)

class NRPAAgent(object):
    def __init__(self, depth, level, gamma=0.8,n=100,PolicyType=1,zeta=1):
        self.depth = depth
        self.level = level
        self.gamma = gamma
        self.n = n
        self.PolicyType = PolicyType
        self.zeta =zeta

    def get_action(self, state):
        return nrpa_actions(state, self.depth, self.level, self.gamma,self.n,self.PolicyType,self.zeta)


class NMCSAgent(object):
    def __init__(self, depth, n_playout, level, heuristic=0, gamma=0.8):
        self.depth = depth
        self.level = level
        self.gamma = gamma
        self.heuristic = heuristic
        self.n_playout = n_playout

    def get_action(self, state):
        return nmcs_actions(state, self.depth, self.n_playout, self.level, self.heuristic, self.gamma)

class FlatMCTreeAgent(object):
    def __init__(self, depth, n, gamma=0.80):
        self.n = n
        self.depth = depth
        self.gamma = gamma

    def get_action(self, state):
        return flat_mc_actions(state, self.depth, self.n, self.gamma)

class BaseTreeSearchAgent(object):
    def __init__(self, returns_distribution=False):
        self.returns_distribution = returns_distribution

    def get_action(self, state):
        indices = tree_search_actions(state, self.depth, self.factor, self.occupation_threshold)
        if self.returns_distribution:
            dist = np.zeros(len(state.actions))
            for index in indices:
                dist[index] = 1
            dist /= dist.sum()
            return dist
        else:
            return random.choice(indices)


class SmallTreeSearchAgent(BaseTreeSearchAgent):
    """
    Average reward per step ~ 1.57
    """
    depth = 4
    factor = 0.22
    occupation_threshold = 0.4


class WideTreeSearchAgent(BaseTreeSearchAgent):
    """
    Average reward per step ~ 2.92
    """
    depth = 3
    factor = 0.22
    occupation_threshold = 0.66


class TsuTreeSearchAgent(BaseTreeSearchAgent):
    """
    Average reward per step ~ 1320
    """
    depth = 3
    factor = 20.0
    occupation_threshold = 0.7


class LargeTreeSearchAgent(BaseTreeSearchAgent):
    """
    Average reward per step ~ 2100
    """
    depth = 3
    factor = 15.0
    occupation_threshold = 0.75


AGENTS = {
    "small": SmallTreeSearchAgent,
    "wide": WideTreeSearchAgent,
    "tsu": TsuTreeSearchAgent,
    "large": LargeTreeSearchAgent,
}
