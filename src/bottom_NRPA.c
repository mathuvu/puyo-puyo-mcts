#include "bitboard.h"
#include "bottom.h"
#include "tall.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include "bitboard.h"
#include "bottom.h"
#include "utils.h"

#define DEATH_VALUE (-10)
#define POLICY_SIZE 10
#define ALPHA 1
#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

struct play  
{  
    int move;
    int color_a, color_b;
};  


int NRPA(puyos_t *board, int n, int level, int depth, double *policy, double gamma, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask, int policyType, double zeta);
int bottom_NRPA(puyos_t *board, int n, int level, int depth, double *policy, double gamma, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask,struct play * sequence, int policyType, double zeta);
int bottom_playoutNRPA(int depth,struct play *sequence, double *policy, double gamma, puyos_t *board, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask, int policyType, double zeta);
void bottom_adaptNRPA(double *policy,struct play *sequence, puyos_t *floor, int num_layers, bitset_t action_mask, int depth, int has_garbage, int num_deals, int policyType);


void bottom_adaptNRPA(double *policy,struct play *sequence, puyos_t *floor, int num_layers, bitset_t action_mask, int depth, int has_garbage, int num_deals, int policyType){
    bitset_t valid;
    bitset_t move;
    //copy of policy
    double *polp;
    int numpairs=0;
    if(policyType){
        numpairs = num_deals * ((num_deals*2)-1);
        polp = calloc(NUM_ACTIONS*numpairs,sizeof(double));
        memcpy(polp, policy, sizeof(double)*NUM_ACTIONS*numpairs);
    }
    else{
        polp = calloc(NUM_ACTIONS,sizeof(double));
        memcpy(polp, policy, sizeof(double)*NUM_ACTIONS);
    }
    //récupérer le state root
    puyos_t *child = malloc(sizeof(puyos_t)*num_layers);
    memcpy(child, floor, sizeof(puyos_t) * num_layers);
    int policySection=0;
    for(int i=0;i<depth;i++){
        int m = sequence[i].move;
        if(policyType){
            int color1 = sequence[i].color_a;
            int color2 = sequence[i].color_b;

            int minColor = min(color1,color2);
            int maxColor = max(color1,color2);
            
            policySection = (minColor * 2 * num_deals) - (minColor/2)*(minColor-1) + maxColor - minColor;
        }
        polp[m] += ALPHA;
        valid = bottom_valid_moves(child, num_layers);
        valid |= valid << (NUM_ACTIONS / 2);
        valid &= action_mask;
        
        double z = 0;
        for(int k = 0; k<NUM_ACTIONS; k++){
            move = 1ULL << k;
            if(!(move&valid)){
                continue;
            }
            z += exp(policy[(policySection*NUM_ACTIONS) +k]);
        }
        for(int k = 0; k<NUM_ACTIONS; k++){
            move = 1ULL << k;
            if(!(move&valid)){
                continue;
            }
            polp[k] -= ALPHA * (exp(policy[(policySection*NUM_ACTIONS) +k]) /z);
        }
        //play
        make_move(child, m, sequence[i].color_a,sequence[i]. color_b);
        bottom_resolve(child, num_layers, has_garbage);
    }
    free(child);
    if(policyType){
        memcpy(policy, polp, sizeof(double)*NUM_ACTIONS* numpairs);
    }
    else{
        memcpy(policy, polp, sizeof(double)*NUM_ACTIONS);
    }
    free(polp);
}

int bottom_playoutNRPA(int depth,struct play *sequence, double *policy, double gamma, puyos_t *board, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask, int policyType, double zeta){
    int num_colors = num_layers - has_garbage, column_full;
    int color_a, color_b;
    puyos_t *child = malloc(sizeof(puyos_t)*num_layers);
    memcpy(child, board, sizeof(puyos_t) * num_layers);
    //puyos_t *base_board = malloc(sizeof(puyos_t)*num_layers);
    //memcpy(base_board, board, sizeof(puyos_t) * num_layers);
    double score = 0;
    bitset_t valid;
    int k;
    int sdepth = depth;
    while(depth>0){
        //Coups valides
        valid = bottom_valid_moves(child, num_layers);
        valid |= valid << (NUM_ACTIONS / 2);
        valid &= action_mask;

        column_full = nb_column_full(board, num_layers);
        if(column_full>0){
            return column_full*TAKE_THE_MAX;
            
        }

        if(!valid)
            return DEATH_VALUE;

        if(num_deals==0){
            chooseColors(&color_a, &color_b, num_colors);
        }else{
            color_a = colors[0]; color_b = colors[1]; num_deals--;
        }
        
        k = chooseMoveNRPA(child, num_layers, valid, policy,num_deals,color_a,color_b,policyType);
        //memcpy(child, base_board, sizeof(puyos_t) * num_layers);
        make_move(child, k, color_a, color_b);
        struct play p = { .move = k, .color_a = color_a, .color_b = color_b };
        sequence[sdepth-depth] = p;
        //Update score
        score += gamma*bottom_resolve(child, num_layers, has_garbage);
        score += zeta*connections_heuristic(child, num_layers);
        gamma *= gamma;
        depth--;
    }
    free(child);//free(base_board);
    return score;
}

int bottom_NRPA(puyos_t *board, int n, int level, int depth, double *policy, double gamma, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask,struct play * sequence, int policyType, double zeta){
    if(level == 0){
        return bottom_playoutNRPA(depth,sequence,policy, gamma, board, num_layers, colors, num_deals, has_garbage, action_mask, policyType,zeta);
    }
    double score, best_score = SMALL_DEATH_VALUE-1;
    struct play * best_seq = (struct play *)calloc(depth ,sizeof(struct play));
    bitset_t move, valid;
    int numpairs = num_deals * ((num_deals*2)-1);
    double *polp;
    if(policyType){
        polp = calloc(NUM_ACTIONS*numpairs,sizeof(double));
        memcpy(polp, policy, sizeof(double)*NUM_ACTIONS*numpairs);
    }
    else{
        polp = calloc(NUM_ACTIONS,sizeof(double));
        memcpy(polp, policy, sizeof(double)*NUM_ACTIONS);
    }
    for(int i=0;i<n;i++){
        int score = bottom_NRPA(board,n, level-1, depth, polp, gamma, num_layers, colors, num_deals, has_garbage, action_mask,sequence, policyType,zeta);
        if(score >= best_score){
            best_score = score;
            //Here
            best_seq = malloc(depth * sizeof(struct play));
            memcpy(best_seq, sequence,depth *  sizeof(struct play));
        }
        bottom_adaptNRPA(polp, sequence, board, num_layers, action_mask,depth,has_garbage, num_deals, policyType);
    }
    memcpy(sequence, best_seq, sizeof(best_seq));
    struct play p = best_seq[0];
    int m = best_seq[0].move;
    free(best_seq);
    free(polp);
    return best_score;
}

int NRPA(puyos_t *board, int n, int level, int depth, double *policy, double gamma, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask, int policyType, double zeta){
    struct play * sequence = (struct play *)calloc(depth ,sizeof(struct play));
    bottom_NRPA(board,n,level,depth,policy,gamma,num_layers,colors,num_deals,has_garbage,action_mask,sequence, policyType,zeta);
    int move = sequence[0].move;
    free(sequence);
    return rand()%2;
    return move;
}