#include <stdlib.h>
#include <math.h>

#include "bitboard.h"
#include "utils.h"
#include "bottom.h"
#include "tall.h"

#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

void chooseColors(int* color_a, int *color_b, int num_colors){
    *color_a = rand()%num_colors;
    *color_b = rand()%num_colors;
}

int nb_column_full(puyos_t *floor, int num_colors){
    puyos_t all = 0;
    for (int i = 0; i < num_colors; ++i) {
        all |= floor[i];
    }
    
    bitset_t result = TOP & all;
    return popcount(result);
}

int chooseMove(puyos_t *floor, int num_layers, bitset_t valid){//Regarder machine de ... avec init aleatoire pour sequence sans remise !
    int k = rand()%NUM_ACTIONS;
    bitset_t move = 1ULL << k;
    while(!(move&valid)){
        k = rand()%NUM_ACTIONS;
        move = 1ULL << k;
    }
    return k;
}

int bottom_group_heuristic(puyos_t *floor, int num_colors) {
    int score = 0;
    for (int i = 0; i < num_colors; ++i) {
        puyos_t layer = floor[i];
        for (int j = 0; j < WIDTH * HEIGHT; j += 2) {
            puyos_t group = flood(3ULL << j, layer);
            layer ^= group;
            int size = popcount(group);
            score += size * size;
        }
    }
    return score;
}

int color_count(puyos_t *floor, int num_layers){
    int score = 0;
    puyos_t kernel = 771ULL;
    for(int k = 0; k<(WIDTH*HEIGHT); k++){
        for(int i = 0; i<num_layers; i++){
            score += popcount((kernel<<k)&floor[i])>0;
        }
        if(score==1){
            score=0;
        }
    }
    return score;
}

int tall_color_count(puyos_t *floor, puyos_t *floor2, int num_layers){
    int score = 0;
    score += color_count(floor, num_layers);
    score += color_count(floor2, num_layers);
    return score;
}

int tall_connections_heuristic(puyos_t *floor, puyos_t *floor2, int num_layers){//HERE TALL
    int score = 0;
    score += connections_heuristic(floor, num_layers);
    score += connections_heuristic(floor2, num_layers);
    return score;
}

int connections_heuristic(puyos_t *floor, int num_layers){//HERE BOTTOM
    int score = 0;
    for(int k = 0; k<num_layers;k++){
        score += connection_count(floor[k]);
    }
    return score;
}

int connection_count(puyos_t floor){
    int score = 0;
    puyos_t kernel_h = 3ULL;
    puyos_t kernel_v = 257ULL;
    for(int k = 0; k<(WIDTH*HEIGHT);k++){
        if((1ULL<<k)&floor){
            score += popcount((kernel_h<<k)&floor) -1;
            score += popcount((kernel_v<<k)&floor) -1;
        }
    }
    return score;
}

int tall_group_heuristic(puyos_t *top, puyos_t *bottom, int num_colors) {
    int score = 0;
    for (int i = 0; i < num_colors; ++i) {
        puyos_t layer[2] = {top[i], bottom[i]};
        for (int k = 0; k < NUM_FLOORS; ++k) {
            for (int j = 0; j < WIDTH * HEIGHT; j += 2) {
                puyos_t group[2] = {0, 0};
                group[k] = 3ULL << j;
                flood_2(group, layer);
                layer[0] ^= group[0];
                layer[1] ^= group[1];
                int size = popcount_2(group);
                score += size * size;
            }
        }
    }
    return score;
}

double bottom_playout(int depth, double gamma, puyos_t *floor, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask, int heuristic){
    int num_colors = num_layers - has_garbage, column_full=0;
    int color_a, color_b;
    double score = 0;
    bitset_t valid;
    int k;
    while(depth>0){
        valid = bottom_valid_moves(floor, num_layers);
        valid |= valid << (NUM_ACTIONS / 2);
        valid &= action_mask;

       column_full = nb_column_full(floor, num_layers);
        if(column_full>0){
            return column_full*TAKE_THE_MAX;
            
        }

        if(num_deals<1){
            chooseColors(&color_a, &color_b, num_colors);
        }else{
            color_a = colors[0]; color_b = colors[1]; num_deals--;
        }
        
        k = chooseMove(floor, num_layers, valid);
        make_move(floor, k, color_a, color_b);
        score += gamma*bottom_resolve(floor, num_layers, has_garbage);
        
        gamma *= gamma;
        depth--;
    }
    if(heuristic){
        score += (double) connections_heuristic(floor, num_colors) - color_count(floor, num_layers)*0.1;
    }
    return score;
}

int chooseMoveNRPA(puyos_t *floor, int num_layers, bitset_t valid, double *policy,int num_deals,int color_a, int color_b, int policyType){
    bitset_t move;
    double z = 0;
    int policySection = 0;
    if(policyType){
        int numpairs = num_deals * ((num_deals*2)-1);
        int minColor = min(color_a,color_b);
        int maxColor = max(color_a,color_b);
        policySection = (minColor * 2 * num_deals) - (minColor/2)*(minColor-1) + maxColor - minColor;
    }
    
    for(int k = 0; k<NUM_ACTIONS; k++){
    move = 1ULL << k;
    if(!(move&valid)){
        continue;
    }
    double v = exp(policy[(policySection * NUM_ACTIONS) + k]);
    z += v;
    }
    double r = ((double) rand() / (RAND_MAX));
    double stop = r*z;
    z = 0;
    int m;
    for(int k = 0; k<NUM_ACTIONS; k++){
        move = 1ULL << k;
        if(!(move&valid)){
            continue;
        }
        z += exp(policy[(policySection * NUM_ACTIONS) + k]);
        if(z>=stop){
            m=k;
            break;
        }
    }
    return m;
}

int chooseMoveGNRPA(puyos_t *floor, int num_layers, bitset_t valid, double *policy,int num_deals,int color_a, int color_b, int policyType, double * beta, double tau){
    bitset_t move;
    double z = 0;
    int policySection = 0;
    if(policyType){
        int numpairs = num_deals * ((num_deals*2)-1);
        int minColor = min(color_a,color_b);
        int maxColor = max(color_a,color_b);
        policySection = (minColor * 2 * num_deals) - (minColor/2)*(minColor-1) + maxColor - minColor;
    }
    
    for(int k = 0; k<NUM_ACTIONS; k++){
    move = 1ULL << k;
    if(!(move&valid)){
        continue;
    }
    double b = 1 - (abs(15-k)/15.0);
    b += connections_heuristic(floor, num_layers)/50.0;
    //z += exp((policy[(policySection * NUM_ACTIONS) + k]/tau)+beta[(policySection * NUM_ACTIONS) + k]);
    z += exp((policy[(policySection * NUM_ACTIONS) + k]/tau)+b);
    }
    double r = ((double) rand() / (RAND_MAX));
    double stop = r*z;
    z = 0;
    int m;
    for(int k = 0; k<NUM_ACTIONS; k++){
        move = 1ULL << k;
        if(!(move&valid)){
            continue;
        }
        double b = 1 - (abs(15-k)/15.0);
        b += connections_heuristic(floor, num_layers)/50.0;
        //z += exp((policy[(policySection * NUM_ACTIONS) + k]/tau)+beta[(policySection * NUM_ACTIONS) + k]);
        z += exp((policy[(policySection * NUM_ACTIONS) + k]/tau)+b);
        if(z>=stop){
            m=k;
            break;
        }
    }
    return m;
}

double tall_playout(int depth, double gamma, puyos_t *floors, int width, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask, int tsu_rules, int heuristic){
    int num_colors = num_layers - has_garbage, column_full = 0;
    int color_a, color_b;
    double score = 0;
    bitset_t valid;
    int k, dummy;
    while(depth>0){
        valid = tall_valid_moves(floors, num_layers, width, tsu_rules);
        valid |= valid << (NUM_ACTIONS / 2);
        valid &= action_mask;

        column_full = nb_column_full(floors, num_layers);
        if(column_full>0){
            return column_full*TAKE_THE_MAX;
            
        }

        if(!valid)
            return TALL_DEATH_VALUE;

        if(num_deals<1){
            chooseColors(&color_a, &color_b, num_colors);
        }else{
            color_a = colors[0]; color_b = colors[1]; num_deals--;
        }
        
        k = chooseMove(floors, num_layers, valid);
        make_move(floors, k, color_a, color_b);
        score += gamma*tall_resolve(floors, num_layers, tsu_rules, has_garbage, &dummy);
        
        gamma *= gamma;
        depth--;
    }
    if(heuristic){
        score += (double) 50*tall_connections_heuristic(floors, floors + num_layers, num_colors) - tall_color_count(floors, floors+num_layers, num_layers);
    }
    return score;
}