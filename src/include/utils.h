#ifndef GYM_PUYOPUYO_UTILS_H_GUARD
#define GYM_PUYOPUYO_UTILS_H_GUARD

#define SMALL_DEATH_VALUE -10000
#define TALL_DEATH_VALUE -10000000
#define TAKE_THE_MAX -1000000000

void chooseColors(int* color_a, int *color_b, int num_colors);
int chooseMove(puyos_t *floor, int num_layers, bitset_t valid);
int nb_column_full(puyos_t *floors, int num_colors);

int bottom_group_heuristic(puyos_t *floor, int num_colors);
int tall_group_heuristic(puyos_t *top, puyos_t *bottom, int num_colors);

double bottom_playout(int depth, double gamma, puyos_t *floor, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask, int heuristic);
double tall_playout(int depth, double gamma, puyos_t *floors, int width, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask, int tsu_rules, int heuristic);

int connection_count(puyos_t floor);
int connections_heuristic(puyos_t *floor, int num_layers);
int tall_connections_heuristic(puyos_t *floor, puyos_t *floor2, int num_layers);
int color_count(puyos_t *floor, int num_layer);
int tall_color_count(puyos_t *floor, puyos_t *floor2, int num_layers);

int chooseMoveNRPA(puyos_t *floor, int num_layers, bitset_t valid, double *policy,int num_deals,int color_a, int color_b, int policyType);
int chooseMoveGNRPA(puyos_t *floor, int num_layers, bitset_t valid, double *policy,int num_deals,int color_a, int color_b, int policyType, double * beta, double tau);

#endif