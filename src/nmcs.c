#include <stdlib.h>
#include <string.h>

#include "bitboard.h"
#include "bottom.h"
#include "tall.h"
#include "utils.h"

double bottom_nmcs(puyos_t *board, int num_layers, int num_deals, int *colors, int has_garbage, bitset_t action_mask, double gamma, int depth, int n_playout, int level, puyos_t *child_buffer, int* best_moves, int *best_colors, int heuristic);
double tall_nmcs(puyos_t *board, int width, int num_layers, int num_deals, int *colors, int has_garbage, int tsu_rules, bitset_t action_mask, double gamma, int depth, int n_playout, int level, int* best_moves, int *best_colors, int heuristic);

double bottom_nmcs(puyos_t *board, int num_layers, int num_deals, int *colors, int has_garbage, bitset_t action_mask, double gamma, int depth, int n_playout, int level, puyos_t *child_buffer, int* best_moves, int *best_colors, int heuristic){
    int num_colors = num_layers - has_garbage, column_full=0;
    double best_score, seq_score = 0, score;
    bitset_t move, valid;
    puyos_t *child = child_buffer;
    do{
        best_score = SMALL_DEATH_VALUE-1;
        valid = bottom_valid_moves(board, num_layers);
        valid |= valid << (NUM_ACTIONS / 2);
        valid &= action_mask;

        column_full = nb_column_full(board, num_layers);
        if(column_full>0){
            seq_score += column_full*TAKE_THE_MAX;
            return seq_score;
        }

        if(num_deals>0){
            best_colors[0] = colors[0];
            best_colors[1] = colors[1];
            for(int k = 0; k<NUM_ACTIONS; k++){
                move = 1ULL << k;
                if(!(move&valid)){
                    continue;
                }
                
                memcpy(child, board, sizeof(puyos_t) * num_layers);
                make_move(child, k, colors[0], colors[1]);
                score = bottom_resolve(child, num_layers, has_garbage);

                if(level == 1){
                    for(int n = 0; n<n_playout;n++){
                        score += bottom_playout(depth - 1, gamma, child, num_layers, colors+2, num_deals-1, has_garbage, action_mask, heuristic);
                    }
                }else{
                    score += bottom_nmcs(child, num_layers, num_deals-1, colors+2, has_garbage, action_mask, gamma, depth-1, n_playout, level-1, child_buffer+num_layers, best_moves+1, best_colors+2, heuristic);
                }
                if(score > best_score){
                    best_score = score;
                    best_moves[0] = k;
                }
            }
            make_move(board, best_moves[0], colors[0], colors[1]);
            seq_score += bottom_resolve(board, num_layers, has_garbage);
            depth--;num_deals--;colors+=2;best_moves++;best_colors+=2;
        }else{
            for (int c0 = 0; c0 < num_colors; ++c0) {
                // Symmetry reduction
                for (int c1 = c0; c1 < num_colors; ++c1) {
                    for(int k = 0; k<NUM_ACTIONS; k++){
                        move = 1ULL << k;
                        if(!(move&valid)){
                            continue;
                        }

                        memcpy(child, board, sizeof(puyos_t) * num_layers);
                        make_move(child, k, c0, c1);
                        score = bottom_resolve(child, num_layers, has_garbage);

                        if(level == 1){
                            for(int n = 0; n<n_playout;n++){
                                score += bottom_playout(depth - 1, gamma, child, num_layers, colors+2, 0, has_garbage, action_mask, heuristic);
                            }
                        }else{
                            score += bottom_nmcs(child, num_layers, 0, colors, has_garbage, action_mask, gamma, depth-1, n_playout, level-1, child_buffer+num_layers, best_moves+1, best_colors+2, heuristic);
                        }
                        if(score > best_score){
                            best_score = score;
                            best_colors[0] = c0;
                            best_colors[1] = c1;
                            best_moves[0] = k;
                        }
                    }
                }
            }
            make_move(board, best_moves[0], best_colors[0], best_colors[1]);
            seq_score += bottom_resolve(board, num_layers, has_garbage);
            depth--;best_moves++;best_colors+=2;
        }
    }while(depth>0 & valid);
    if(heuristic){
        seq_score += (double) 10*connections_heuristic(board, num_colors) - color_count(board, num_layers);
    }
    return seq_score;
}

double tall_nmcs(puyos_t *board, int width, int num_layers, int num_deals, int *colors, int has_garbage, int tsu_rules, bitset_t action_mask, double gamma, int depth, int level, int n_playout, int* best_moves, int *best_colors, int heuristic){
    int num_colors = num_layers - has_garbage, dummy, column_full=0;
    double best_score, seq_score = 0, score;
    puyos_t *child = malloc(sizeof(puyos_t)*num_layers*NUM_FLOORS), *base_board = malloc(sizeof(puyos_t)*num_layers*NUM_FLOORS);
    bitset_t move, valid;
    memcpy(base_board, board, sizeof(puyos_t) * num_layers * NUM_FLOORS);
    do{
        best_score = TALL_DEATH_VALUE-1;
        valid = tall_valid_moves(base_board, num_layers, width, tsu_rules);
        valid |= valid << (NUM_ACTIONS / 2);
        valid &= action_mask;

        column_full = nb_column_full(board, num_layers);
        if(column_full>0){
            seq_score += column_full*TAKE_THE_MAX;
            return seq_score;
        }

        if(num_deals>0){
            best_colors[0] = colors[0];
            best_colors[1] = colors[1];
            for(int k = 0; k<NUM_ACTIONS; k++){
                move = 1ULL << k;
                if(!(move&valid)){
                    continue;
                }
                
                memcpy(child, base_board, sizeof(puyos_t) * num_layers * NUM_FLOORS);
                make_move(child, k, colors[0], colors[1]);
                score = tall_resolve(child, num_layers, tsu_rules, has_garbage, &dummy);

                if(level == 1){
                    for(int n = 0; n<n_playout;n++){
                        score += tall_playout(depth - 1, gamma, child, width, num_layers, colors+2, num_deals-1, has_garbage, action_mask, tsu_rules, heuristic);
                    }
                }else{
                    score += tall_nmcs(child, width, num_layers, num_deals-1, colors+2, has_garbage, tsu_rules, action_mask, gamma, depth-1, n_playout, level-1, best_moves+1, best_colors+2, heuristic);
                }
                if(score > best_score){
                    best_score = score;
                    best_moves[0] = k;
                }
            }
            make_move(base_board, best_moves[0], colors[0], colors[1]);
            seq_score += tall_resolve(base_board, num_layers, tsu_rules, has_garbage, &dummy);
            depth--;num_deals--;colors+=2;best_moves++;best_colors+=2;
        }else{
            for (int c0 = 0; c0 < num_colors; ++c0) {
                // Symmetry reduction
                for (int c1 = c0; c1 < num_colors; ++c1) {
                    for(int k = 0; k<NUM_ACTIONS; k++){
                        move = 1ULL << k;
                        if(!(move&valid)){
                            continue;
                        }

                        memcpy(child, base_board, sizeof(puyos_t) * num_layers * NUM_FLOORS);
                        make_move(child, k, c0, c1);
                        score = tall_resolve(child, num_layers, tsu_rules, has_garbage, &dummy);

                        if(level == 1){
                            for(int n = 0; n<n_playout;n++){
                                score += tall_playout(depth - 1, gamma, child, width, num_layers, colors+2, num_deals-1, has_garbage, action_mask, tsu_rules, heuristic);
                            }
                        }else{
                            score += tall_nmcs(child, width, num_layers, num_deals, colors, has_garbage, tsu_rules, action_mask, gamma, depth-1, n_playout, level-1, best_moves+1, best_colors+2, heuristic);
                        }
                        if(score > best_score){
                            best_score = score;
                            best_colors[0] = c0;
                            best_colors[1] = c1;
                            best_moves[0] = k;
                        }
                    }
                }
            }
            make_move(base_board, best_moves[0], best_colors[0], best_colors[1]);
            seq_score += tall_resolve(base_board, num_layers, tsu_rules, has_garbage, &dummy);
            depth--;best_moves++;best_colors+=2;
        }
    }while(depth>0 & valid);
    if(heuristic){
        seq_score += (double) 50*tall_connections_heuristic(board, board + num_layers, num_colors) - tall_color_count(board, board+num_layers, num_layers);
    }
    return seq_score;
}
