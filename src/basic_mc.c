#include <stdlib.h>
#include <string.h>

#include "bitboard.h"
#include "bottom.h"
#include "tall.h"
#include "utils.h"

int bottom_flat_mc(int n, int depth, double gamma, puyos_t *floor, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask){
    double score, best_score = SMALL_DEATH_VALUE;
    int best_move = 0;
    bitset_t move, valid;
    valid = bottom_valid_moves(floor, num_layers);
    valid |= valid << (NUM_ACTIONS / 2);
    valid &= action_mask;

    puyos_t *child = malloc(sizeof(puyos_t)*num_layers), *tmp = malloc(sizeof(puyos_t)*num_layers);
    for(int k = 0; k<NUM_ACTIONS; k++){
        move = 1ULL << k;
        if(!(move&valid)){
            continue;
        }
        memcpy(child, floor, sizeof(puyos_t) * num_layers);

        make_move(child, k, colors[0], colors[1]);
        score = bottom_resolve(child, num_layers, has_garbage);
        for(int i = 0; i<n; i++){
            memcpy(tmp, child, sizeof(puyos_t) * num_layers);
            score += bottom_playout(depth, gamma, tmp, num_layers, colors+2, num_deals-1, has_garbage, action_mask, 0);
        }
        score /= n;
        if(score>best_score){
            best_score = score;
            best_move = k;
        }
    }
    free(child);
    free(tmp);
    return best_move;
}

int tall_flat_mc(int n, int depth, double gamma, puyos_t *floor, int width, int num_layers, int* colors, int num_deals, int has_garbage, bitset_t action_mask, int tsu_rules){
    double score, best_score = TALL_DEATH_VALUE;
    int best_move = -1;
    bitset_t move, valid;
    valid = tall_valid_moves(floor, num_layers, width, tsu_rules);
    valid |= valid << (NUM_ACTIONS / 2);
    valid &= action_mask;

    puyos_t *child = malloc(sizeof(puyos_t)*num_layers*NUM_FLOORS), *tmp = malloc(sizeof(puyos_t)*num_layers*NUM_FLOORS);
    for(int k = 0; k<NUM_ACTIONS; k++){
        move = 1ULL << k;
        if(!(move&valid)){
            continue;
        }
        memcpy(child, floor, sizeof(puyos_t) * num_layers*NUM_FLOORS);

        make_move(child, k, colors[0], colors[1]);
        score = bottom_resolve(child, num_layers, has_garbage);
        for(int i = 0; i<n; i++){
            memcpy(tmp, child, sizeof(puyos_t) * num_layers*NUM_FLOORS);
            score += tall_playout(depth, gamma, tmp, width, num_layers, colors+2, num_deals-1, has_garbage, action_mask, tsu_rules, 0);
        }
        if(score>best_score){
            best_score = score;
            best_move = k;
        }
    }
    free(child);
    free(tmp);
    return best_move;
}